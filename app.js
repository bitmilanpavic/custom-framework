const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
mongoose.connect('mongodb://proba:proba@ds229790.mlab.com:29790/proba');
const Persons = require('./models/persons');


// Cors
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Z-Key");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});


// bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// List persons
app.post('/api/persons', (req, res) => {
    let gender = (req.body.gender) ? req.body.gender : { '$regex': '' };

    Persons.find({
        gender: gender,
        first_name: { '$regex': req.body.name },
        last_name: { '$regex': req.body.lastname },
        age: { '$regex': req.body.age }
    })
    .then((data) => {
        res.json(data);
    })
});


// Get single person
app.post('/api/person', (req, res) => {   
    Persons.findOne({_id: req.body._id})
    .then((data) => {
        res.json(data);
    });
});


// Update single person
app.put('/api/person/', (req, res) => {
    Persons.findOne({_id: req.body._id}, (err, data)=>{
        data.first_name = req.body.name;
        data.last_name = req.body.lastname;
        data.email = req.body.email;
        data.gender = req.body.gender;
        data.age = req.body.age;
        data.ip_address = req.body.ip_address;
        data.save();
        res.json({edited:true});
    })
});


// Delete person
app.delete('/api/person', (req, res) => {
    Persons.findByIdAndRemove(req.body._id, function(err, person){
        res.json({deleted:true})           
    }).exec();
});


// Add new person
app.post('/api/savePerson', (req, res) => {
    let data = {
        first_name: req.body.name,
        last_name: req.body.lastname,
        email: req.body.email,
        gender: req.body.gender,
        age: req.body.age,
        ip_address: req.body.ip_address
    }
    let newPerson = new Persons(data);

    newPerson.save((err, person) => {
        if (err) {
            res.json({ error: true });
            return;
        }
        res.json({ created: true });
    });
});


const port = process.env.PORT || 8081;
app.listen(port);