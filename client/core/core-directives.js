import globals from "./core-global-state";


/**
 * This method is used to bind event to element 
 * @param {*} element 
 */
export const bindDirective = function (element) {
    if (element.getAttribute('c-bind')) {
        let eventAndHandler = element.getAttribute('c-bind').split('=');

        element.addEventListener(eventAndHandler[0], (event) => {
            this[eventAndHandler[1]](event);
        });
    }
}



/**
 * This method is used to repeat elements based on some array, it can repeat normal element or component
 * @param [] cFor Array containing elements that have cFor attribute
 * @param {*} state Component state
 */
export const forDirective = function (cFor, state) {
    if (cFor.length) {
        cFor.forEach((cFor) => {
            let parent = cFor.parent;
            let child = cFor.child;
            let childcFor = child.getAttribute('c-for');
            let component = child.getAttribute('component');
            let finalRepeaterString = '';
            let repeaterLength = (new Function('state, globals', `return ${childcFor}.length`)(state, globals));

            // Check if cfor contains globals
            if (childcFor.indexOf('globals') > -1) {
                if (!globals.instances.includes(this)) {
                    globals.instances.push(this);
                }
            }


            // If we are repeating component
            if (component) {
                let match;
                let childText = child.innerText;
                childcFor = new Function('state, globals', `return ${childcFor}`).call(this, this.state, globals);
                let componentName = null;
                const attributes = [];

                let temporaryChildComponentDom = childText.replace('{', '<');
                temporaryChildComponentDom = temporaryChildComponentDom.replace('}', '/>');
                temporaryChildComponentDom = document.createRange().createContextualFragment(temporaryChildComponentDom);

                Array.prototype.slice.call(temporaryChildComponentDom.children[0].attributes).forEach((attribute) => {
                    let temporaryArray = [];
                    temporaryArray.push(attribute.name, attribute.value);
                    attributes.push(temporaryArray);
                });

                let props = attributes.reduce((acc, el) => {
                    acc[el[0]] = el[1];
                    return acc;
                }, {});

                while (match = /{([A-Z].+?)\s/.exec(childText)) {
                    childText = childText.replace(match[0], '');
                    componentName = match[1];
                }

                if (!repeaterLength) {
                    this.Observer.subscribe(child.getAttribute('c-for'), "cForComponent", parent, child, '', '', '', this, props, componentName);
                    child.innerHTML = '';
                    parent.innerHTML = '';
                    return;
                }

                child.innerHTML = '';
                parent.innerHTML = '';

                for (let i = 0; i < repeaterLength; i++) {
                    this.childComponents.forEach((childComponent) => {
                        if (childComponent[componentName]) {
                            let thisChildProps = { ...props }

                            for (let key in thisChildProps) {
                                thisChildProps[key] = childcFor[i][key] || (new Function(`state, globals`, `return ${thisChildProps[key]}`)(this.state, globals));
                            }

                            let component = new childComponent[componentName](thisChildProps);
                            this.childComponentsInstances.push(component);

                            let childClone = child.cloneNode(true);
                            childClone.appendChild(component.parsedTemplate);
                            parent.appendChild(childClone);

                            // state, type, elementNode, templateString, attrs, index numofTemplateStrings, instance
                            this.Observer.subscribe(child.getAttribute('c-for'), "cForComponent", parent, child.innerText, '', '', '', this, props, componentName);
                        }
                    })
                }
                return;
            }


            // If we are repeating non component just normal element
            let initialMatch;
            let childHTMLString = child.outerHTML;
            let numOfTemplateStrings = 0;

            while (initialMatch = /{[^-](.+?)}/.exec(childHTMLString)) {
                childHTMLString = childHTMLString.replace(initialMatch[0], '');
                numOfTemplateStrings++;
            }

            for (let i = 0; i < repeaterLength; i++) {
                finalRepeaterString += child.outerHTML;
            }

            let match;
            let a = 0;
            let b = 0;
            while (match = /{([^-].+?)}/.exec(finalRepeaterString)) {
                a++;
                finalRepeaterString = finalRepeaterString.replace(match[0], (new Function(`b, state, childcFor, globals`, `return ${childcFor}[${b}].${match[1]}`)(b, state, childcFor, globals)))

                if (a % numOfTemplateStrings == 0) {
                    b++;
                }
            }

            // state, type, elementNode, templateString, attrs, index numofTemplateStrings, instance
            this.Observer.subscribe(childcFor, "cFor", parent, child.outerHTML, '', '', numOfTemplateStrings, this);
            parent.innerHTML = finalRepeaterString;

            // Loop true new repeater DOM and replace template strings with new values
            const addEvents = (parent) => {
                if (parent) {
                    for (let i = 0; i < parent.childNodes.length; i++) {
                        let element = parent.childNodes[i];

                        if (element.nodeType == document.TEXT_NODE) {
                            let match;
                            let elementTextNodeValue = element.nodeValue;
                            let templateString = elementTextNodeValue;
                            templateString = templateString.replace('{-', '{{');
                            templateString = templateString.replace('-}', '}}');

                            while (match = /{-\s*(.+?)\s*-}/g.exec(elementTextNodeValue)) {
                                const elementParent = element.parentNode;

                                // Replace template string {{state.name...}} in DOM with real value
                                elementTextNodeValue = elementTextNodeValue.replace(match[0], (new Function('state, props, globals', 'return ' + match[1])(this.state, this.props, globals)));
                                element.nodeValue = elementTextNodeValue;

                                // Map state to elements
                                this.Observer.subscribe(match[1], "textNode", elementParent, templateString, '', i, '', this);
                            }
                        }

                        if (element.nodeType == document.ELEMENT_NODE) {
                            let match;
                            let matches = [];

                            // Change element attr with state or props values
                            Array.prototype.slice.call(element.attributes).forEach((attribute) => {
                                if (attribute.name != 'c-bind' && attribute.name != 'c-for') {
                                    while (match = /{-\s*(.+?)\s*-}/.exec(attribute.value)) {
                                        matches.push({ state: match[1], attr: attribute.name });
                                        element.setAttribute(attribute.name, (new Function(`state, props, globals`, `return ${match[1]}`)(this.state, this.props, globals)));
                                    }
                                }
                            });

                            // Reduce array to object
                            matches = matches.reduce((acc, el) => {
                                acc[el.state] = acc[el.state] || [];
                                acc[el.state].push(el.attr);
                                return acc;
                            }, {});


                            // Subscribe for each state
                            Object.entries(matches).forEach((match) => {
                                this.Observer.subscribe(match[0], "attr", element, '', match[1], '', this);
                            });

                            bindDirective.call(this, element);
                        }
                        addEvents(element);
                    }
                }

            }

            addEvents(parent);
        });
    }
}