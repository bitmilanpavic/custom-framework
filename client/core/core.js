import { forDirective, bindDirective } from './core-directives';
import Observer from './core-observer';
import globals from './core-global-state';

export default class Core {
    /**
     * @constructor 
     * @param {template, childComponents, props, state, globalsState} settings
     * configuration for each component, globals.state should be defined in first component to load
     */
    constructor(settings) {
        this.childCounter = 0;
        this.childComponentsProps = [];
        this.cForElements = [];
        this.childComponentsInstances = [];

        this.template = settings.template;
        this.childComponents = settings.childComponents || null;
        this.props = settings.props || null;
        this.state = settings.state;
        globals.state = (settings.globals) ? settings.globals.state : globals.state;

        this.Observer = Observer(); // Initiate observer
        this.generateChildComponentsProps(); // Generate props data for each component
        this.parsedTemplate = this.createComponentDom(this.template); // Parse template
        forDirective.call(this, this.cForElements, this.state); // Add repeater directive
        this.checkForGlobalState(); // Check if component have global sate

        globals.router = (settings.globals) ? settings.globals.router : globals.router;
        this.haveRoutes = null;
        if (settings.globals) {
            this.haveRoutes = (settings.globals.router != undefined) ? settings.globals.router : null;
        }

        // ComponentDidMount lifecycle
        if (this.componentDidMount) {
            this.componentDidMount();
        };

        this.determineRoute(); // Managa routes
        console.log(this.Observer, 'Observer');
    }



    determineRoute() {
        if (globals.router.length) {
            let path = window.location.hash;
            path = path.replace(/[?].+/g,'');
            
            if (this.haveRoutes) {
                globals.router.forEach((route) => {
                    if (route.path == path) {
                        let component = route.component;
                        component = new component({}).parsedTemplate;
                        globals.routeElement.appendChild(component);
                    }
                });

                window.onhashchange = (event) => {
                    let path = window.location.hash;
                    path = path.replace(/[?].+/g,'');
                    globals.router.forEach((route) => {
                        if (route.path == path) {
                            let component = route.component;
                            component = new component({}).parsedTemplate;

                            globals.routeElement.innerHTML = '';
                            globals.routeElement.appendChild(component);
                            globals.routeElement.replaceWith(globals.routeElement);
                        }
                    });
                }
            }
        }
    }



    /**
     * We use this method to update state and update DOM
     * @param {*} newState 
     * @param boolen isGlobal
     */
    setState(newState, isGlobal = false) {
        if (isGlobal) {
            globals.state = {
                ...globals.state,
                ...newState
            }

            for (let state in newState) {
                globals.instances.forEach((instance) => {
                    instance.Observer.emit(`globals.state.${state}`, state, this.state, this.props);
                });
            }
            return;
        }


        // Update state and props
        this.state = {
            ...this.state,
            ...newState
        }

        if (this.props) {
            this.props = {
                ...this.props,
                ...newState
            }
        }


        // For each new state thats added emit event and child event if child components exists
        for (let state in newState) {
            this.Observer.emit(`state.${state}`, state, this.state, this.props);
            this.Observer.emit(`props.${state}`, state, this.state, this.props);

            if (this.childComponents) {
                this.childComponentsInstances.forEach((childComponentsInstance) => {
                    childComponentsInstance.setState(newState);
                });
            }
        }
    }



    /**
     * This method is used to create component DOM
     */
    createComponentDom() {
        const componentDom = document.createRange().createContextualFragment(this.template);

        const parseDom = (componentDom) => {
            let match;
            if (componentDom) {
                for (var i = 0; i < componentDom.childNodes.length; i++) {
                    let element = componentDom.childNodes[i];

                    // If element type is TEXT NODE
                    if (element.nodeType == document.TEXT_NODE) {
                        this.elementTypeIsText(element, i);
                    }

                    // If element type is ELEMENT NODE
                    if (element.nodeType == document.ELEMENT_NODE) {
                        this.elementTypeIsElement(element);
                    }

                    parseDom(element);
                }
            }
            return componentDom;
        }
        return parseDom(componentDom);
    }



    /**
     * This method replaces template strings with state or props values 
     * @param DOM element 
     * @param index child element index compared to parent
     */
    elementTypeIsText(element, index) {
        let match;
        let elementTextNodeValue = element.nodeValue;
        let templateString = elementTextNodeValue;

        while (match = /{{\s*(.+?)\s*}}/g.exec(elementTextNodeValue)) {
            const elementParent = element.parentNode;

            // Replace template string {{state.name...}} in DOM with real value
            elementTextNodeValue = elementTextNodeValue.replace(match[0], (new Function('state, props, globals', 'return ' + match[1])(this.state, this.props, globals)));
            element.nodeValue = elementTextNodeValue;

            // Map state to elements
            this.Observer.subscribe(match[1], "textNode", elementParent, templateString, '', index, '', this);
        }
    }



    /**
     * This method replaces template strings in element attributes with state or props values
     * @param DOM element 
     */
    elementTypeIsElement(element) {
        let match;
        let matches = [];

        if (element.id == 'router') {
            globals.routeElement = element;
        }

        // Change element attr with state or props values
        Array.prototype.slice.call(element.attributes).forEach((attribute) => {
            if (attribute.name != 'c-bind' && attribute.name != 'c-for') {
                while (match = /{{\s*(.+?)\s*}}/.exec(attribute.value)) {
                    matches.push({ state: match[1], attr: attribute.name });
                    element.setAttribute(attribute.name, (new Function(`state, props, globals`, `return ${match[1]}`)(this.state, this.props, globals)));
                }
            }
        });


        // Reduce array to object
        matches = matches.reduce((acc, el) => {
            acc[el.state] = acc[el.state] || [];
            acc[el.state].push(el.attr);
            return acc;
        }, {});


        // Subscribe for each state
        Object.entries(matches).forEach((match) => {
            this.Observer.subscribe(match[0], "attr", element, '', match[1], '', this);
        });


        // Initiate child components if we have some
        if (this.childComponents) {
            let childComponentTag = element.tagName;
            childComponentTag = childComponentTag.charAt(0).toUpperCase() + childComponentTag.slice(1).toLowerCase();

            this.childComponents.forEach((childComponent) => {
                if (childComponent[childComponentTag]) {
                    let component = new childComponent[childComponentTag](this.childComponentsProps[this.childCounter]);

                    this.childComponentsInstances.push(component);
                    element.parentNode.replaceWith(component.parsedTemplate.childNodes[0]);
                    this.childCounter++;
                }
            });
        }


        // Directives actions 
        if (element.getAttribute('c-for')) {
            this.cForElements.push({ 'parent': element.parentNode, 'child': element });
        }

        bindDirective.call(this, element);
    }



    /**
     * This method populates array with props data for child components
     */
    generateChildComponentsProps() {
        let templateCopy = this.template;
        let match;

        while (match = /(<\s*[A-Z].+?\s*\/>)/.exec(templateCopy)) {
            let props = {};
            templateCopy = templateCopy.replace(match[1], '');
            let componentTemplateString = match[1];
            let temporaryDom = document.createRange().createContextualFragment(componentTemplateString);

            Array.prototype.slice.call(temporaryDom.childNodes[0].attributes).forEach((attribute) => {
                props[attribute.name] = new Function('', `return this.${attribute.value}`).call(this);
            });

            this.childComponentsProps.push(props);
        }
    }



    /**
    * This method store current component instance in array if it has globalsState
    */
    checkForGlobalState() {
        Object.entries(this.Observer.states).forEach((entry) => {
            if (/^globals.state/.test(entry[0])) {
                if (!globals.instances.includes(this)) {
                    globals.instances.push(this);
                }
            }
        });
    }



    /**
     * This is a helper method for ajax calls
     * @param string method
     * @param string url
     * @param {*}    data
     */
    ajax(method, url, data = null) {
        return new Promise((resolve, reject) => {
            var xhr = new XMLHttpRequest();

            xhr.open(method, url);
            // xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    resolve(xhr.response);
                } else {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                }
            }

            xhr.onerror = function () {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }

            xhr.send(JSON.stringify(data));
        });
    }

} // End Core