import globals from './core-global-state';
import { bindDirective } from './core-directives';

const Observer = function () {
    return {
        states: {},


        
        /**
         * Subscribe method is used to store elements that needs to be updated when state changes
         */
        subscribe: function (state, type, elementNode, templateString, attrs, index, numOfTemplateStrings, instance, props, componentName) {
            this.states[state] = this.states[state] || [];

            // If we are changing text node in DOM add relative info
            if (type == 'textNode') {
                this.states[state].push({ element: elementNode, templateString, type, index, instance });
            }
            
            // If we are changing attribute in element add relative info
            if (type == 'attr') {
                this.states[state].push({ element: elementNode, attrs, type, instance });
            }

            // If we are repeating component
            if (type == 'cForComponent') {
                this.states[state].push({ element: elementNode, type, props, cForValue: state, componentName, instance, child:templateString });
            }

            // If we are repeating normal element
            if (type == 'cFor') {
                this.states[state].push({ element: elementNode, type, templateString, cForValue: state, numOfTemplateStrings, instance });
            }
        },



        /**
         *  Emit method is used to update DOM when state is changed
         */
        emit: function (stateKey, stateAlias, state, props, childComponents) {
            if (this.states[stateKey] !== undefined) {

                // Value of states[key] is array
                // We loop true array and depending if we are changing attribute or text node
                // we are updating DOM
                this.states[stateKey].forEach(element => {

                    if (element.type == 'attr') {
                        element.attrs.forEach((attr) => {
                            element.element.setAttribute(attr, state[stateAlias] || props[stateAlias] || globals.state[stateAlias]);
                        });
                    }


                    if (element.type == 'textNode') {
                        let templateString = element.templateString;
                        let match;

                        while (match = /{{\s*(.+?)\s*}}/.exec(templateString)) {
                            templateString = templateString.replace(match[0], (new Function('state, props, globals', 'return ' + match[1])(state, props, globals)));
                        }

                        element.element.childNodes[element.index].nodeValue = templateString;
                    }


                    if (element.type == 'cForComponent') {
                        let instance = element.instance;
                        let childcFor = (new Function('state, globals', `return ${element.cForValue}`)(state, globals));
                        let repeaterLength = childcFor.length;
                        let child =  element.child;                        
                       
                        element.element.innerHTML = '';
                        child.innerHTML = '';
                        
                        for (let i = 0; i < repeaterLength; i++) {
                            instance.childComponents.forEach((childComponent) => {
                                if (childComponent[element.componentName]) {
                                    let thisChildProps = { ...element.props }

                                    for (let key in thisChildProps) {
                                        thisChildProps[key] = childcFor[i][key] || (new Function(`state, globals`, `return ${thisChildProps[key]}`)(state, globals));
                                    }

                                    let component = new childComponent[element.componentName](thisChildProps);
                                    instance.childComponentsInstances.push(component);

                                    let childClone = child.cloneNode(true);
                                    childClone.appendChild(component.parsedTemplate);
                                    element.element.appendChild(childClone);
                                }
                            });
                        }

                    }


                    if (element.type == 'cFor') {
                        let cForValue = (new Function('globals, state', `return ${element.cForValue}`)(globals, state));
                        let numOfTemplateStrings = element.numOfTemplateStrings;
                        let finalRepeaterString = '';

                        for (let i = 0; i < cForValue.length; i++) {
                            finalRepeaterString += element.templateString;
                        }

                        let a = 0;
                        let b = 0;
                        let match;
                        let childcFor = element.cForValue;
                        while (match = /{([^-].+?)}/.exec(finalRepeaterString)) {
                            a++;
                            finalRepeaterString = finalRepeaterString.replace(match[0], (new Function(`b, state, childcFor, globals`, `return ${childcFor}[${b}].${match[1]}`)(b, state, childcFor, globals)))

                            if (a % numOfTemplateStrings == 0) {
                                b++;
                            }
                        }

                        let parent = element.element;
                        parent.innerHTML = '';
                        let childrens = document.createRange().createContextualFragment(finalRepeaterString);
                        parent.appendChild(childrens);
                        element.element.replaceWith(parent);

                        // Loop true new cfor dom and add events
                        const addEvents = (parent, instance) => {
                            if (parent) {
                                for (let i = 0; i < parent.childNodes.length; i++) {
                                    let element = parent.childNodes[i];

                                    if (element.nodeType == document.TEXT_NODE) {
                                        let match;
                                        let elementTextNodeValue = element.nodeValue;
                                        let templateString = elementTextNodeValue;
                                        templateString = templateString.replace('{-', '{{');
                                        templateString = templateString.replace('-}', '}}');

                                        while (match = /{-\s*(.+?)\s*-}/g.exec(elementTextNodeValue)) {
                                            const elementParent = element.parentNode;

                                            // Replace template string {{state.name...}} in DOM with real value
                                            elementTextNodeValue = elementTextNodeValue.replace(match[0], (new Function('state, props, globals', 'return ' + match[1])(instance.state, instance.props, globals)));
                                            element.nodeValue = elementTextNodeValue;

                                            // Map state to elements
                                            instance.Observer.subscribe(match[1], "textNode", elementParent, templateString, '', i, '', instance);
                                        }
                                    }

                                    if (element.nodeType == document.ELEMENT_NODE) {
                                        let match;
                                        let matches = [];

                                        // Change element attr with state or props values
                                        Array.prototype.slice.call(element.attributes).forEach((attribute) => {
                                            if (attribute.name != 'c-bind' && attribute.name != 'c-for') {
                                                while (match = /{-\s*(.+?)\s*-}/.exec(attribute.value)) {
                                                    matches.push({ state: match[1], attr: attribute.name });
                                                    element.setAttribute(attribute.name, (new Function(`state, props, globals`, `return ${match[1]}`)(instance.state, instance.props, globals)));
                                                }
                                            }
                                        });

                                        // Reduce array to object
                                        matches = matches.reduce((acc, el) => {
                                            acc[el.state] = acc[el.state] || [];
                                            acc[el.state].push(el.attr);
                                            return acc;
                                        }, {});


                                        // Subscribe for each state
                                        Object.entries(matches).forEach((match) => {
                                            instance.Observer.subscribe(match[0], "attr", element, '', match[1], '', instance);
                                        });

                                        bindDirective.call(instance, element);
                                    }
                                    addEvents(element, instance);
                                }
                            }

                        }

                        addEvents(element.element, element.instance);
                    }

                });
            }
        }
    }
}

export default Observer;