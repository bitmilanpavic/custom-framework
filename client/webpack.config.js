const path = require('path');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    mode: 'development',
    // watch: true,
    optimization:{
        // minimize: true
    },
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'index.bundle.js',
        publicPath: '/build'
    },
    resolve: {
        extensions: ['.js']
    },
    module: {
        rules: [
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        attrs: [':data-src']
                    }
                }
            },
            {
               test: /\.js$/,
               loader: 'babel-loader',
               exclude: /node_modules/  
            },
            {
                test: /\.(s*)css$/,   
                use: [
                {
                    loader: 'style-loader'  
                },    
                {
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1  
                    }
                },
                {
                    loader:'postcss-loader',
                    options: {
                        ident: 'postcss',
                        plugins: (loader) => [
                            autoprefixer({ browsers: [">1%", "last 20 versions"] })
                        ]
                    }
                },
                {
                    loader:'sass-loader'    
                }]
            },
            {
                test: /\.(png|jpe?g|gif)$/,
                loader: 'url-loader?limit=8000&name=images/[name].[ext]' 
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({filename: 'style.css'})
    ]
}