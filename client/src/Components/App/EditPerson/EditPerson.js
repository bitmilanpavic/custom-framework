import Core from '../../../../core/core';
import EditPerson_template from './EditPerson_template.html';

class EditPerson extends Core {
    constructor(props) {
        super({
            template: EditPerson_template,
            props,
            state: {
                _id:'',
                name: '',
                lastname: '',
                email: '',
                gender: '',
                age: '',
                ip_address: '',
                personEdited: ''
            }
        });
    }

    componentDidMount(){
        let params = window.location.hash;
        params = params.slice(params.indexOf('?') + 1, params.length);
        params = params.split('=');
        
        this.ajax("POST", "http://localhost:8081/api/person", { _id : params[1] } )
        .then((result) => JSON.parse(result))
        .then((result) => {
            if(!result.error){
                this.setState({
                    _id: result._id,
                    name: result.first_name,
                    lastname: result.last_name,
                    email: result.email,
                    gender: result.gender,
                    age: result.age,
                    ip_address: result.ip_address
                });

                // Select correct radio type
                document.querySelectorAll('input[type="radio"]').forEach(function(element){
                   if (element.getAttribute('checkedValue') == element.value) {
                       element.setAttribute('checked', '');
                   }
                });
            }else{
                throw 'Error';
            }
        });
    }

    editPerson(event) {
        this.ajax("PUT", "http://localhost:8081/api/person", this.state)
        .then((result) => JSON.parse(result))
        .then((result) => {
            if(result.edited){
                this.setState({personEdited: 'Person edited...'});
            }else{
                throw 'Error';
            }
        });
    }

    radioValue(event) {
        this.setState({ gender: event.target.value });
    }

    inputsValue(event) {
        this.setState({ [event.target.name]: event.target.value });
    }
}

export default EditPerson;