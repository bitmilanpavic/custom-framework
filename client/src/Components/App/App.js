import Core from '../../../core/core';
import App_template from './App_template.html';
import Home from './Home/Home';
import AddPerson from './AddPerson/AddPerson';
import EditPerson from './EditPerson/EditPerson';

class App extends Core {
    constructor() {
        super({
            template: App_template,
            globals: {
                state: {
                   people:[]
                },
                router: [
                    { path: "", component: Home },
                    { path: "#add-person", component: AddPerson },
                    { path: "#edit-person", component: EditPerson }
                ]
            },
            state: {
                proba:1
            }
        });
    }
}

export default App;