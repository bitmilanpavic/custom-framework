import Core from '../../../../core/core';
import AddPerson_template from './AddPerson_template.html';

class AddPerson extends Core {
    constructor(props) {
        super({
            template: AddPerson_template,
            props,
            state: {
                name: '',
                lastname: '',
                email: '',
                gender: '',
                age: '',
                ip_address: '',
                personCreated:''
            }
        });
    }

    addNewPerson(){
        this.ajax("POST", "http://localhost:8081/api/savePerson", this.state)
        .then((result) => JSON.parse(result))
        .then((result) => {
            if(result.created){
                this.setState({personCreated: 'Person created...'});
            }else{
                throw 'Error';
            }
        });
    }

    radioValue(event) {
        this.setState({ gender: event.target.value });
    }

    inputsValue(event) {
        this.setState({ [event.target.name]: event.target.value });
    }
}

export default AddPerson;