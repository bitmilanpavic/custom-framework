import Core from '../../../../core/core';
import Home_template from './Home_template.html';
import personImage from './person.jpg';
import globals from '../../../../core/core-global-state';

class Home extends Core {
    constructor(props) {
        super({
            template: Home_template,
            props,
            state: {
                name: '',
                lastname: '',
                age: '',
                gender: '',
                personImage
            }
        });
    }

    removePerson(event) {        
        event.preventDefault();

        this.ajax("DELETE", "http://localhost:8081/api/person", {_id: event.target.getAttribute('_id')})
        .then((result) => JSON.parse(result))    
        .then((result) => {
           if(result.deleted){
               event.target.closest('li').remove();
           }else{
               throw 'Person not deleted';
           }
        });
    }

    componentDidMount() {
        this.ajax("POST", "http://localhost:8081/api/persons", this.state)
        .then((result) => JSON.parse(result))
        .then((result) => {        
            this.setState({ people: result }, true);
        });
    }

    searchForPeople() {
        this.ajax("POST", "http://localhost:8081/api/persons", this.state)
        .then((result) => JSON.parse(result))
        .then((result) => {        
            this.setState({ people: result }, true);
        });
    }

    radioValue(event) {
        this.setState({ gender: event.target.value });
        this.searchForPeople();
    }

    inputsValue(event) {
        this.setState({ [event.target.name]: event.target.value });
        this.searchForPeople();
    }
}

export default Home;