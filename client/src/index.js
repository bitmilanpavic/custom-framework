import Core from '../core/core'; 
import App from './Components/App/App'; 
import './style.scss';
import globals from '../core/core-global-state';

const app = new App();
const root = document.getElementById('root');

root.appendChild(app.parsedTemplate);


