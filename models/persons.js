const mongoose = require('mongoose');

const Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
 
const personsSchema = new Schema({
    first_name: String,
    last_name: String,
    email: String,
    gender: String,
    age: String,
    ip_address: String
});

const Persons = mongoose.model('persons', personsSchema);
module.exports = Persons;